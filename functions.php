<?php

use BookStack\Actions\Webhook;
use BookStack\Entities\Models\Page;
use BookStack\Facades\Theme;
use BookStack\Theming\ThemeEvents;


Theme::listen(ThemeEvents::WEBHOOK_CALL_BEFORE, function (string $event, Webhook $webhook, $detail) {

    // Add a condition to only run this custom logic for specific webhook calls.
    if ($webhook->name === 'Debug Webhook') {
        // Build and return the data which will be sent as JSON to the endpoint.
        $webhookData = [
            'the_event' => $event,
        ];

        if ($detail instanceof Page) {
            $webhookData['text'] = 'The page updated was named: ' . $detail->name;
        }

        return $webhookData;
    }

    return null;
});